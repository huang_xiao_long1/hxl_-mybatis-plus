package com.HXL;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.HXL.mapper.UserMapper;
import com.HXL.pojo.user;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisplusWrapper {

    @Autowired
    private UserMapper userMapper;
    @Test
    void contextLoads() {
        QueryWrapper<user> wrapper=new QueryWrapper<>();
        wrapper.isNotNull("name");
        wrapper.isNotNull("email");
        wrapper.gt("age",20);
        List<user> users = userMapper.selectList(wrapper);
        users.forEach(System.out::println);


    }


}
