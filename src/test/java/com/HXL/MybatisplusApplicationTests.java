package com.HXL;

import com.HXL.mapper.UserMapper;
import com.HXL.pojo.user;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class MybatisplusApplicationTests {

    @Autowired
    private UserMapper userMapper;
    @Test
    void contextLoads() {
        List<user> users = userMapper.selectList(null);
        for (user user : users) {
            System.out.println(user);
        }

    }
    @Test
    void inserttest(){
        user user = new user();
        user.setAge(22);
        user.setName("wuhu");
        user.setEmail("2360108734@qq.com");
        int insert = userMapper.insert(user);

    }

    @Test
    void updateTest(){
        user user=new user();
        user.setId(6l);
        user.setAge(18);
        user.setName("qifei");
        user.setEmail("15970766498");
        userMapper.updateById(user);
    }

    @Test
    void  versionTest(){
        //查询用户信息
        user user = userMapper.selectById(1l);
        //修改用户信息
        System.out.println(user.getId());
        user.setId(1l);
        user.setName("HXL");
        user.setEmail("123456789@qq.com");
        userMapper.updateById(user);

    }

    @Test
    void deltedtest(){
        userMapper.deleteById(1l);
    }

}
