package com.HXL;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.po.TableFill;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

import java.util.ArrayList;

public class autoCode {
    public static void main(String[] args) {
        //需要构建一个代码自动生成器对象
        AutoGenerator mpg=new AutoGenerator();
        //配置生成策略
        //1.全局配置
        GlobalConfig gc=new GlobalConfig();
        String projectPath=System.getProperty("user.dir");
        gc.setOutputDir(projectPath+"/src/main/java");//设置代码生成的位置
        gc.setAuthor("黄小龙");//设置作者
        gc.setOpen(false);//是否打开输出目录(false为否)
        gc.setFileOverride(false);//是否覆盖文件
        gc.setServiceImplName("%sService");//去掉Service的I前缀
        gc.setIdType(IdType.ID_WORKER);
        gc.setDateType(DateType.ONLY_DATE);
        gc.setSwagger2(true);
        mpg.setGlobalConfig(gc);

        //2.设置数据源
        DataSourceConfig dsc=new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/mybatisplus? useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
        dsc.setDriverName("com.mysql.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("1508111260qqq");
        dsc.setDbType(DbType.MYSQL);
        mpg.setDataSource(dsc);
        //3.包的设置
        PackageConfig pc=new PackageConfig();
        pc.setModuleName("myModel");
        pc.setParent("com.HXL");
        pc.setEntity("entity");//实体类包，相当于pojo
        pc.setMapper("mapper");
        pc.setController("controller");
        pc.setService("service");
        mpg.setPackageInfo(pc);

        //4.策略配置

        StrategyConfig strategyConfig=new StrategyConfig();
        strategyConfig.setInclude("user");//设置要映射的表名，一般要该的也就是这里
        // 数据库表映射到实体的命名策略:下划线转驼峰
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        // 数据库表字段映射到实体的命名策略, 未指定按照 naming 执行
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setEntityLombokModel(true);//是否为lombok模型（默认 false
        strategyConfig.setLogicDeleteFieldName("deleted");

        //自动填充配置
        TableFill gmtCreat=new TableFill("gmt_create",FieldFill.INSERT);
        TableFill gmtModified=new TableFill("gmtModified",FieldFill.INSERT_UPDATE);
        ArrayList<TableFill> tableFills=new ArrayList<>();
        tableFills.add(gmtCreat);
        tableFills.add(gmtModified);
        strategyConfig.setTableFillList(tableFills);
        //乐观锁
        strategyConfig.setVersionFieldName("version");
        strategyConfig.setRestControllerStyle(true);
        strategyConfig.setControllerMappingHyphenStyle(true);//localhost:8080//hello_id_2
        mpg.setStrategy(strategyConfig);
        mpg.execute();
    }
}
