package com.HXL.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.HXL.pojo.user;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends BaseMapper<user> {

    @Select("select * from user")
    List<user> select();


}
