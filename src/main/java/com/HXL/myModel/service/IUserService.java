package com.HXL.myModel.service;

import com.HXL.myModel.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黄小龙
 * @since 2021-05-27
 */
public interface IUserService extends IService<User> {

}
