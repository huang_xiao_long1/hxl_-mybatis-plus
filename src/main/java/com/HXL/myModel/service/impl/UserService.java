package com.HXL.myModel.service.impl;

import com.HXL.myModel.entity.User;
import com.HXL.myModel.mapper.Mapper;
import com.HXL.myModel.service.IUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黄小龙
 * @since 2021-05-27
 */

@Service
public class UserService extends ServiceImpl<Mapper, User> implements IUserService {

}
