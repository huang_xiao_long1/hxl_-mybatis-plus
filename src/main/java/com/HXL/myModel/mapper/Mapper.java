package com.HXL.myModel.mapper;

import com.HXL.myModel.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 黄小龙
 * @since 2021-05-27
 */
@Repository
public interface Mapper extends BaseMapper<User> {

}
